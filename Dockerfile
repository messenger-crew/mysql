FROM mysql:8.0.0

#ENV MYSQL_ROOT_PASSWORD temp123!

COPY entrypoint /docker-entrypoint-initdb.d

EXPOSE 3306:3306

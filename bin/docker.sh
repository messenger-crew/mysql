#!/usr/bin/env sh

INFO_FILE=${INFO_FILE:-"app.info"};
DIR=$(dirname $0)
version="./bin/semver.sh"
run="./bin/run.sh"

. $DIR/../$INFO_FILE

start () {
  if [ "$(is_running)" == yes ]
  then
    echo "Container already running"
  else
    echo "checking container $CONTAINER_NAME ..."
    if [ "$(check_container)" == exists ]
    then
      echo "starting container $CONTAINER_NAME ..."
      start_container
    else
      echo "checking image $APP_NAME ..."
      if [ "$(check_image)" == "not exists" ]
      then
        build
      fi
      echo "run $CONTAINER_NAME ..."
      $run
    fi
  fi
}

build () {
    echo "docker build -t "$IMAGE_NAME
    docker build -t $IMAGE_NAME .
}

check_image () {
    if docker images | grep -Eq "^$APP_NAME\s+$VERSION"
    then
        echo exists
    else
        echo not exists
       # pull
    fi
}


check_container () {
    if docker ps -a | grep -Eq "\s+$CONTAINER_NAME$"
    then
        echo exists
    else
        echo not exists
    fi
}

is_running () {
    if docker ps | grep -Eq "\s+$CONTAINER_NAME$"
    then
        echo yes
    else
        echo no
    fi
}

set_version () {
  VERSION=$(. $version)
  IMAGE_NAME=${APP_NAME}:$VERSION
  CONTAINER_NAME=${APP_NAME}"_v$VERSION"
  #REMOTE_IMAGE_NAME=$REMOTE_IMAGE:$REMOTE_TAG
}


start_container () {
  docker start $CONTAINER_NAME
}

stop_all () {
  echo "Stopping all $APP_NAME"
  local running=$(docker ps -q -f name=$APP_NAME)
  if [ -n "$running" ];
  then
    docker stop $running
  fi
}

stop () {
    docker stop $CONTAINER_NAME
}

set_version

key="$1";

shift $(( $# > 0 ? 1 : 0  ))

set_version

case $key in
    status)
      echo image $REMOTE_IMAGE_NAME is $(check_image)
      echo container $CONTAINER_NAME is $(check_container)
      echo running: $(is_running)
    ;;
    version)
      . $version "$@"
    ;;
    reset)
      stop
      remove
      start
    ;;
    restart)
      restart
    ;;
    start)
      start
    ;;
    stop)
      stop
    ;;
    stop_all)
      stop_all
    ;;
esac

#Messenger MYSQL server

##Quick start

- Install Docker on your device
- ```git clone git@bitbucket.org:messenger-crew/mysql.git```
- ```cd ./mysql```
- ```./bin/docker.sh start```

##How to manage directly
- Check container id ```docker ps```
- Get access to container terminal ```docker exec -ti <container_id> bash```
- ```mysql -u root -p$MYSQL_ROOT_PASSWORD main```